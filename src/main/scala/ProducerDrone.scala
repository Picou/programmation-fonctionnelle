import java.io._
import java.util.Properties
import org.apache.kafka.clients.producer._
import spray.json._
import DefaultJsonProtocol._
import DroneItemImplicits._

object ProducerDrone extends App {

  def droneTask() = {
    val drone = new Drone();
    val droneItem = drone.makeRandDrone()
    val toSend = droneItem.toJson.compactPrint
    val record = new ProducerRecord[String, String]("drone", "", toSend)
		println(toSend)
    producer.send(record)
    
  }

	val properties = new Properties()
  properties.put("bootstrap.servers", "localhost:9092")
  properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  val producer = new KafkaProducer[String, String](properties)
  val timer = new java.util.Timer()
  val task = new java.util.TimerTask {
    def run() = droneTask()
  }
  timer.schedule(task, 1000L, 1000L)
	
  scala.io.StdIn.readLine()
	producer.close()
	task.cancel();
  
}