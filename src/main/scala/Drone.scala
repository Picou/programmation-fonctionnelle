import java.text.SimpleDateFormat
import java.util.Calendar
import spray.json._
import DefaultJsonProtocol._

case class DroneItem(
    numberPlate: String,
    lat: Int,
    lng: Int,
    time: Long
)

object DroneItemImplicits extends DefaultJsonProtocol {
	implicit val impDrone = jsonFormat4(DroneItem)
}

class Drone {
    
  def makeRandDrone() : DroneItem = {
		val time = Calendar.getInstance().getTime().getTime
		val uuid = java.util.UUID.randomUUID.toString
		val r = scala.util.Random
		val lat = r.nextInt(50)
		val lng = r.nextInt(50)
		new DroneItem(uuid, lat, lng, time)
	}
}

