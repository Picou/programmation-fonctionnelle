import scala.collection.JavaConverters._
import java.util.regex.Pattern
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark
import org.apache.spark._
import org.apache.spark.streaming._
//import org.apache.spark.streaming.StreamingContext
//import org.apache.spark.streaming.StreamingContext._
//import java.util.{Collections, Properties}

object SparkConsumer extends App {

  // composants spark
  val conf = new SparkConf().setAppName("ConsumerSpark").setMaster("local[*]")
  val ssc = new StreamingContext(conf, Seconds(15)) //interval de batch

  val kafkaParams = Map[String, Object](
    "bootstrap.servers" -> "localhost:9092",
    "key.deserializer" -> classOf[StringDeserializer],
    "value.deserializer" -> classOf[StringDeserializer],
    "group.id" -> "consumer-spark",
    "auto.offset.reset" -> "latest",
    "enable.auto.commit" -> (false: java.lang.Boolean)
  )

  val topics = Array("drone")
  val stream = KafkaUtils.createDirectStream[String, String](
    ssc,
    LocationStrategies.PreferConsistent,
    ConsumerStrategies.Subscribe[String, String](topics, kafkaParams)
  )

  //stream.map(record => (record.key, record.value))
  stream.foreachRDD(rawRDD => {
    //récupère

    //TODO faire un traitement si le temps
    val rdd = rawRDD.map(record => (record.key, record.value))
    
    //spark.json(rdd).write.mode(SaveMode.Append).csv("out/drone.csv")
    rdd.coalesce(1).saveAsTextFile("drone")
  })

  
}